#include <stdio.h>
#include "hello.decl.h"

class Main : public CBase_Main {
public: 
	Main(CkArgMsg* m) {
		CkPrintf("Hello World!\n");
		CkExit();
	};
};

#include "hello.def.h"
