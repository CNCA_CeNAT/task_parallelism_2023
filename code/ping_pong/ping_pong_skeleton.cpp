#include <stdio.h>
#include "ping_pong.decl.h"

CProxy_Singleton aProxy, bProxy;

class Main : public CBase_Main {
	public: Main(CkArgMsg* m) {
		aProxy = CProxy_Singleton::ckNew();
		bProxy = CProxy_Singleton::ckNew();
		aProxy.start();
	};
};

class Singleton : public CBase_Singleton {
public: 
	Singleton() {
	};
	void start(){
		// TO DO: implement this method
	}
	void ping(){
		// TO DO: implement this method
	}
	void pong(){
		// TO DO: implement this method
	}
};
#include "ping_pong.def.h"
