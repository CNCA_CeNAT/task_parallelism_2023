#include <stdio.h>
#include "ping_pong.decl.h"

CProxy_Singleton aProxy, bProxy;

class Main : public CBase_Main {
	public: Main(CkArgMsg* m) {
		aProxy = CProxy_Singleton::ckNew();
		bProxy = CProxy_Singleton::ckNew();
		aProxy.start();
	};
};

class Singleton : public CBase_Singleton {
public: 
	Singleton() {
	};
	void start(){
		bProxy.ping();
	}
	void ping(){
		aProxy.pong();
	}
	void pong(){
		CkPrintf("Done!\n");
		CkExit();
	}
};
#include "ping_pong.def.h"
