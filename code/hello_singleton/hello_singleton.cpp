#include <stdio.h>
#include "hello_singleton.decl.h"

CProxy_Singleton objectProxy;

class Main : public CBase_Main {
public: 
	Main(CkArgMsg* m) {
		objectProxy = CProxy_Singleton::ckNew();
		objectProxy.hello();
	};
};

class Singleton : public CBase_Singleton {
public: 
	Singleton() {
	};
	void hello(){
		CkPrintf("Hello World!\n");
		CkExit();
	};
};
#include "hello_singleton.def.h"
