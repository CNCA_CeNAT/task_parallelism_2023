HPC Week 2023
Task Parallelism
Instructor Esteban Meneses
emeneses@cenat.ac.cr

Most of the content of these code samples were provided courtesy of Prof. Laxmikant V. Kale from the Parallel Programming Laboratory (PPL) of the University of Illinois at Urbana-Champaign. The PPL originally developed the ideas behind parallel object programming and has maintained the Charm++ code base for more than 25 years.

